static class AssemblyLine
{
    private static int productionRate = 221;
    public static double SuccessRate(int speed)
    {
        switch(speed)
        {
            case > 0 and <= 4:
            return 1;

            case > 4 and <= 8:
            return 0.9f;

            case 9:
            return 0.8f;

            case 10:
            return 0.77;

            default:
            return 0;
        }
    }
    
    public static double ProductionRatePerHour(int speed)
    {
        return SuccessRate(speed) * speed * productionRate;
    }

    public static int WorkingItemsPerMinute(int speed)
    {
        return (int)(SuccessRate(speed) * speed * productionRate)/60;
    }
}
