using System.Text;

public static class RunLengthEncoding
{
    public static string Encode(string input)
    {
        var encodedString = new StringBuilder();
        var repeatCount = 0;
        for (int i = 0, length = input.Length; i < length; i++)
        {
            var currentElement = input[i];
            var nextElementIndex = (i + 1);

            if ((nextElementIndex) < length && currentElement == input[nextElementIndex])
            {
                repeatCount++;
            }
            else
            {
                if (repeatCount != 0)
                    encodedString.Append(repeatCount + 1);
                encodedString.Append(currentElement);
                repeatCount = 0;
            }
        }
        return encodedString.ToString();
    }

    public static string Decode(string input)
    {
        var decodeString = new StringBuilder();
        var updateIteration = 0;
        for (int i = 0, length = input.Length; i < length; i++)
        {
            var element = input[i];
            if (element >= '1' && element <= '9')
            {
                var nextElement = input[i + 1];
                var iteration = (int)(element - '0');

                if (IsNotNUmber(nextElement))
                {
                    if (updateIteration == 0)
                        updateIteration += iteration;
                    for (int j = 1; j <= updateIteration - 1; j++)
                    {
                        decodeString.Append(nextElement);
                    }
                }
                else
                {
                    if (updateIteration == 0)
                        updateIteration = DigitMultiplier(iteration);
                    else
                    {
                        iteration = updateIteration;
                        updateIteration = DigitMultiplier(iteration);
                    }
                    updateIteration += (int)(nextElement - '0');
                }
            }
            if (IsNotNUmber(element))
            {
                updateIteration = 0;
                decodeString.Append(element);
            }
        }
        return decodeString.ToString();
    }

    private static int DigitMultiplier(int value)
    {
        var multiplier = 10;
        return value * multiplier;
    }

    private static bool IsNotNUmber(int value)
    {
        return (value >= 'a' && value <= 'z') || (value >= 'A' && value <= 'Z') || value == ' ';
    }
}