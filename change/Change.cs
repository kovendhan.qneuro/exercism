using System;
using System.Collections.Generic;

public static class Change
{
    public static int[] FindFewestCoins(int[] coins, int target)
    {
        var headList = new List<List<int>>();
        var len = coins.Length;
        var smallestNumber = int.MaxValue;
        var storeTarget = target;

        if (target < 0 || (target < coins[0] && target != 0))
        {
            throw new ArgumentException();
        }

        for (int j = len - 1; j >= 0; j--)
        {
            var changeList = new List<int>();
            while (storeTarget != 0 && storeTarget > 0)
            {
                var matchingCoin = 0;
                var currentCoinIndex = 0;
                for (int i = 0; i <= j; i++)
                {
                    if (coins[i] <= storeTarget)
                    {
                        matchingCoin = coins[i];
                        currentCoinIndex = i;
                    }
                }

                storeTarget -= matchingCoin;
                changeList.Add(matchingCoin);

                if (storeTarget < coins[0] && storeTarget != 0)
                {
                    changeList[^1] = coins[currentCoinIndex - 1];
                    storeTarget += (changeList[^1] - coins[currentCoinIndex - 1]);
                }

                if (storeTarget == 0)
                {
                    changeList.Sort();
                    if (changeList.Count < smallestNumber)
                    {
                        smallestNumber = changeList.Count;
                    }
                    headList.Add(changeList);
                }
            }

            storeTarget = target;
        }

        for (int k = 0; k < headList.Count; k++)
        {
            if (headList[k].Count == smallestNumber)
            {
                return headList[k].ToArray();
            }
        }
        return new int[0];
    }
}