﻿public interface IAnimal
{
    void MakeSound();
}

class Dog : IAnimal
{
    public void MakeSound() => Console.WriteLine("Bow...Bow..Bow..");

    public void Run() {}
}

class Cat : IAnimal
{
    public void MakeSound() => Console.WriteLine("Meeeew....");
    
    public void Eat() {}
}

class Cow
{
    public void makeSound() => Console.WriteLine("Moooo....");
}

class Sound
{
    public void Play(IAnimal animal) => animal.MakeSound();
}
public class Program
{
    public static void Main()
    {
        IAnimal dogg = new Dog();
        IAnimal catt = new Cat();

        Dog dog = new();
        // Cat cat = new();
        // Cow cow = new();
        Sound sound = new Sound();

        sound.Play(dogg);
        sound.Play(catt);
        // sound.Play(cow);

        
    }
}
