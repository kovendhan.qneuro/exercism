using System.Collections.Generic;

public class HighScores
{
    private List<int> list = new();

    private int latest = 0;

    public HighScores(List<int> list) 
    {
        this.list = list;
        latest = list[list.Count-1];
    }

    public List<int> Scores() => list;

    public int Latest() => latest;

    public int PersonalBest()
    {
        var max = int.MinValue;
        foreach(var item in list)
        {
            if(item > max)
            max = item;
        }
        return max;
    }

    public List<int> PersonalTopThree()
    {
        list.Sort();
        list.Reverse();
        var nTopCount = 3;
        
        if(list.Count < 3 )
        {
            nTopCount = list.Count;
        }
        return list.GetRange(0,nTopCount);
    }
}