using System;

static class SavingsAccount
{
    public static float InterestRate(decimal balance)
    {
        switch(balance){
            case < 0:
            return 3.213f;
            case < 1000:
            return 0.5f;
            case >= 1000 and < 5000:
            return 1.621f;
            case >= 5000:
            return 2.475f;
        }
     //   throw new NotImplementedException("Please implement the (static) SavingsAccount.InterestRate() method");
    }

    public static decimal Interest(decimal balance)
    {
        return ((decimal)(InterestRate(balance) / 100)) * balance;
        //throw new NotImplementedException("Please implement the (static) SavingsAccount.Interest() method");
    }

    public static decimal AnnualBalanceUpdate(decimal balance)
    {
        return Interest(balance) + balance;
       // throw new NotImplementedException("Please implement the (static) SavingsAccount.AnnualBalanceUpdate() method");
    }

    public static int YearsBeforeDesiredBalance(decimal balance, decimal targetBalance)
    {
        var count = 0;
        while(balance <= targetBalance){
            balance += Interest(balance);
            ++count;
        }
        return count;
        //throw new NotImplementedException("Please implement the (static) SavingsAccount.YearsBeforeDesiredBalance() method");
    }
}
