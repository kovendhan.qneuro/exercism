using System.Collections.Generic;

public static class SumOfMultiples
{
    public static int Sum(IEnumerable<int> multiples, int max)
    {
        var result = 0;
        var collections = new HashSet<int>();

        for (int i = 1; i < max; i++)
        {
            foreach (var val in multiples)
            {
                if (val != 0 && i % val == 0)
                {
                    collections.Add(i);
                }
            }
        }

        foreach (var item in collections)
        {
            result += item;
        }

        return result;
    }
}