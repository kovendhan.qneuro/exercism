public static class BeerSong
{
    public static string Recite(int startBottles, int takeDown)
    {
        var verseCollector = new string[takeDown];
        for (int i = startBottles, length = startBottles - takeDown, j = 0; i > length; i--)
        {
            var result = i switch
            {
                0 => "No more bottles of beer on the wall, no more bottles of beer.\n" +
            "Go to the store and buy some more, 99 bottles of beer on the wall.",
                1 => "1 bottle of beer on the wall, 1 bottle of beer.\n" +
            "Take it down and pass it around, no more bottles of beer on the wall.",
                2 => "2 bottles of beer on the wall, 2 bottles of beer.\n" +
            "Take one down and pass it around, 1 bottle of beer on the wall.",
                _ => $"{i} bottles of beer on the wall, {i} bottles of beer.\n" +
            $"Take one down and pass it around, {i - 1} bottles of beer on the wall."
            };
            verseCollector[j] = result;
            j++;
        }
        return string.Join("\n\n", verseCollector);
    }
}