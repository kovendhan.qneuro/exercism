using System;

public static class DifferenceOfSquares
{
    public static int CalculateSquareOfSum(int max)
    {
        var result = 0;
        for (int i = 1; i <= max; i++)
        {
            result += i;
        }
        return result * result;
    }

    public static int CalculateSumOfSquares(int max)
    {
        var result = 0;
        for (int i = 1; i <= max; i++)
        {
            result += i * i;
        }
        return result;
    }

    public static int CalculateDifferenceOfSquares(int max)
    {
        return CalculateSquareOfSum(max) - CalculateSumOfSquares(max);
    }
}