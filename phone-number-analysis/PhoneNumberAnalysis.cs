public static class PhoneNumber
{
    public static (bool IsNewYork, bool IsFake, string LocalNumber) Analyze(string phoneNumber)
    {
        var collection = phoneNumber.Split("-");

        var firstSet = (collection[0] == "212");
        var secondSet = (collection[1] == "555");
        var thirdSet = collection[2];

        return (firstSet, secondSet, thirdSet);
    }

    public static bool IsFake((bool IsNewYork, bool IsFake, string LocalNumber) phoneNumberInfo)
    {
        return phoneNumberInfo.IsFake;
    }
}
