using System.Text;

public static class Raindrops
{
    public static string Convert(int number)
    {
        var numCollection = new int[3] { 3, 5, 7 };
        var wordCollection = new string[3] { "Pling", "Plang", "Plong" };
        var output = new StringBuilder();

        for (int i = 0, length = numCollection.Length; i < length; i++)
        {
            if (number % numCollection[i] == 0)
            {
                output.Append(wordCollection[i]);
            }
        }

        if (output.ToString().Equals(string.Empty))
        {
            return number.ToString();
        }
        return output.ToString();
    }
}