using System;

public class Queen
{
    public int Row { get; }
    public int Column { get; }

    public Queen(int row, int column)
    {
        Row = row;
        Column = column;
    }
}

public static class QueenAttack
{
    public static bool CanAttack(Queen white, Queen black)
    {
        var wRow = white.Row;
        var bRow = black.Row;
        var wColumn = white.Column;
        var bColumn = black.Column;
        if (wRow == bRow || bColumn == wColumn)
            return true;
        if (wRow + wColumn == bRow + bColumn)
            return true;
        if (wRow > wColumn)
        {
            wRow -= wColumn;
            wColumn -= wColumn;
        }
        else
        {
            wColumn -= wRow;
            wRow -= wRow;
            while (wColumn <= 7)
            {
                if (bColumn == wColumn && bRow == wRow)
                    return true;
                wColumn++;
                wRow++;
            }
        }
        return false;
    }

    public static Queen Create(int row, int column)
    {
        if ((row >= 0 && row <= 7) && (column >= 0 && column <= 7))
            return new Queen(row, column);
        throw new ArgumentOutOfRangeException();
    }
}