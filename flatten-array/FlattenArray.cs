using System.Collections;
using System.Collections.Generic;

public static class FlattenArray
{
    public static IEnumerable Flatten(IEnumerable input)
    {
        var collection = new List<int>();

        foreach (var items in input)
        {
            if (items != null)
            {
                if (items.GetType().IsArray)
                {
                    collection.AddRange((IEnumerable<int>)Flatten((IEnumerable)items));
                }
                else
                {
                    collection.Add((int)items);
                }
            }
        }
        return collection;
    }
}