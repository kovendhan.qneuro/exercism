public static class Darts
{
    public static int Score(double x, double y)
    {
        //Applying pythagorus formula x^2 + y^2 = r^2
        var radius = (x*x) + (y*y);
        var innerRadius = 1;
        var middleRadius = 5;
        var outerRadius = 10;
        var inCirclePoints = 10;
        var midCirclePoints  = 5;
        var outCirclePoints  = 1;
        if (radius <= innerRadius)
            return inCirclePoints;
        if (radius <= middleRadius * middleRadius)
            return midCirclePoints;
        if (radius <= outerRadius * outerRadius)
            return outCirclePoints;
        return 0;
    }
}
