public static class Luhn
{
    public static bool IsValid(string numbers)
    {
        var sumOfNumbers = 0;
        numbers = numbers.Replace(" ", "");
        var numberLength = numbers.Length;

        if (numberLength <= 1)
            return false;

        for (int i = numberLength - 1; i >= 0; i--)
        {
            if ((numbers[i] >= '0' && numbers[i] <= '9'))
            {
                var number = (int)((numbers[i]) - '0');
                if ((numberLength - 1) % 2 == 0)
                {
                    if (i % 2 != 0)
                    {
                        number *= 2;
                        if (number > 9)
                            number -= 9;
                        sumOfNumbers += number;
                    }
                    else
                        sumOfNumbers += number;
                }
                else
                {
                    if (i % 2 == 0)
                    {
                        number *= 2;
                        if (number > 9)
                            number -= 9;
                        sumOfNumbers += number;
                    }
                    else
                        sumOfNumbers += number;
                }
            }
            else
                return false;
        }
        return sumOfNumbers % 10 == 0;
    }
}