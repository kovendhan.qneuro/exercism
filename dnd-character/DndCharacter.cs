using System;
using System.Collections.Generic;

public class DndCharacter
{
    public int Strength { get; private set; }

    public int Dexterity { get; private set; }

    public int Constitution { get; private set; }

    public int Intelligence { get; private set; }

    public int Wisdom { get; private set; }

    public int Charisma { get; private set; }

    public int Hitpoints { get; private set; }

    public static int Modifier(int score)
    {
        var initialPoints = 10;
        //formula to find Modifier based on score
        return (int)Math.Floor((decimal)(score - initialPoints) / 2);
    }

    public static int Ability()
    {
        var sumOfCollections = 0;
        var numOfDices = 4;
        var minDiceValue = 1;
        var maxDiceValue = 7;
        var random = new Random();
        var diceCollections = new List<int>();
        for (int i = 1; i <= numOfDices; i++)
        {
            diceCollections.Add(random.Next(minDiceValue, maxDiceValue));
        }
        diceCollections.Sort();
        diceCollections.Reverse();
        for (int i = 0, length = diceCollections.Count - 1; i < length; i++)
        {
            sumOfCollections += diceCollections[i];
        }
        return sumOfCollections;
    }

    public static DndCharacter Generate()
    {
        var character = new DndCharacter
        {
            Strength = Ability(),
            Dexterity = Ability(),
            Constitution = Ability(),
            Intelligence = Ability(),
            Wisdom = Ability(),
            Charisma = Ability(),
        };
        character.Hitpoints = 10 + Modifier(character.Constitution);
        return character;
    }
}
