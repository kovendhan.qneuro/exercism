using System.Collections.Generic;

public static class MatchingBrackets
{
    public static bool IsPaired(string input)
    {
        var stack = new Stack<char>();

        for (int i = 0, length = input.Length; i < length; i++)
        {
            var bracketCollection = "{}[]()";
            if (bracketCollection.Contains(input[i]))
            {
                switch (input[i])
                {
                    case '(':
                        stack.Push(')');
                        break;
                    case '{':
                        stack.Push('}');
                        break;
                    case '[':
                        stack.Push(']');
                        break;
                    default:
                        if (stack.Count != 0 && stack.Peek() == input[i])
                        {
                            stack.Pop();
                        }
                        else
                        {
                            return false;
                        }
                        break;
                }
            }
        }
        return stack.Count == 0;
    }
}
