using System;

public class Player
{
    private Random random = new Random();
    public int RollDie()
    {
        return random.Next(1, 18);
    }

    public double GenerateSpellStrength()
    {
        return random.NextDouble() + random.Next(0, 100);
    }
}
