public static class RotationalCipher
{
    public static string Rotate(string text, int shiftKey)
    {
        var cipherText = new char[text.Length];
        for (int i = 0, length = text.Length; i < length; i++)
        {
            var cipherNumber = 0;
            //Using ASCII decimal values for Captial letter Alphabets.
            if ((text[i] >= 'A' && text[i] <= 'Z'))
            {
                cipherNumber = (int)text[i] + shiftKey;
                if (cipherNumber > 'Z')
                {
                    //performing mod formula calculation on rotation.
                    cipherNumber %= 26 + ('A' - 1);
                    cipherNumber += ('A' - 1);
                }
                cipherText[i] = (char)cipherNumber;
            }
            else if (text[i] >= 'a' && text[i] <= 'z')
            {
                //Using ASCII decimal values for Small letter Alphabets.
                cipherNumber = (int)text[i] + shiftKey;
                if (cipherNumber > 'z')
                {
                    //performing mod formula calculation on rotation.
                    cipherNumber %= 26 + ('a' - 1);
                    cipherNumber += ('a' - 1);
                }
                cipherText[i] = (char)cipherNumber;
            }
            else
            {
                cipherText[i] = text[i];
            }
        }
        return new string(cipherText);
    }
}