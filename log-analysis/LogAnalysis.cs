public static class LogAnalysis
{
    public static string SubstringAfter(this string str, string s)
    {
        return str.Substring(str.IndexOf(s) + s.Length);
    }

    public static string SubstringBetween(this string str, string str1, string str2)
    {
        var position1 = str.IndexOf(str1) + str1.Length;
        var position2 = str.IndexOf(str2);
        return str.Substring(position1, position2 - position1);
    }

    public static string Message(this string name) => name.SubstringAfter(":").Trim();

    public static string LogLevel(this string name) => name.SubstringBetween("[", "]");
}