using System;
using System.Collections.Generic;

public static class Sieve
{
    public static int[] Primes(int limit)
    {
        var collectionList = new List<int>();

        if (limit < 0)
            throw new ArgumentOutOfRangeException();

        for (int i = 2; i <= limit; i++)
        {
            collectionList.Add(i);
        }

        for (int j = 2; j <= limit; j++)
        {
            for (int k = 2; k <= collectionList.Count; k++)
            {
                var removeItem = (j * k);
                if (removeItem > limit)
                    break;
                collectionList.Remove(removeItem);
            }
        }
        return collectionList.ToArray();
    }
}