using System;

public class PhoneNumber
{
    public static string Clean(string phoneNumber)
    {
        phoneNumber = phoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace("+", "").Replace(".", "").Replace(" ", "");

        string num = "1234567890";

        if (phoneNumber.Length < 10 || (phoneNumber.Length >= 11 && phoneNumber[0] != '1'))
        {
            throw new ArgumentException();
        }

        if (phoneNumber.Length >= 11 && phoneNumber[0] == '1')
        {
            phoneNumber = phoneNumber.Remove(0, 1);
        }

        for (int i = 0, length = phoneNumber.Length; i < length; i++)
        {
            if (!num.Contains(phoneNumber[i]))
            {
                throw new ArgumentException();
            }
        }

        if (phoneNumber[0] == '0' || phoneNumber[0] == '1' || phoneNumber[3] == '0' || phoneNumber[3] == '1')
        {
            throw new ArgumentException();
        }

        return phoneNumber;
    }
}