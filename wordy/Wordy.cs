using System;

public static class Wordy
{
    public static int Answer(string question)
    {
        var commonReplaceList = new string[] { "What is ", "?" };

        question = question.Replace("plus", "+").Replace("minus", "-").Replace("multiplied by", "*").Replace("divided by", "/");

        for (int i = 0, length = commonReplaceList.Length; i < length; i++)
        {
            question = question.Replace(commonReplaceList[i], "");
        }

        question.TrimStart();
        var splitedList = question.Split(" ");

        if (splitedList.Length == 1)
        {
            var operand = 0;
            var isPossible = int.TryParse((splitedList[0]), out operand);

            return isPossible ? operand : throw new ArgumentException();
        }

        if (splitedList.Length == 3)
        {
            var operand1 = 0;
            var operand2 = 0;

            var isPossible1 = int.TryParse((splitedList[0]), out operand1);
            var isPossible2 = int.TryParse((splitedList[2]), out operand2);

            if (isPossible1 && isPossible2)
            {
                switch (splitedList[1])
                {
                    case "+":
                        return operand1 + operand2;
                    case "-":
                        return operand1 - operand2;
                    case "*":
                        return operand1 * operand2;
                    case "/":
                        return operand1 / operand2;
                    default:
                        throw new ArgumentException();

                }
            }
            else
            {
                throw new ArgumentException();
            }

        }

        if (splitedList.Length == 5)
        {
            var operand1 = 0;
            var operand2 = 0;
            var operand3 = 0;

            var isPossible1 = int.TryParse((splitedList[0]), out operand1);
            var isPossible2 = int.TryParse((splitedList[2]), out operand2);
            var isPossible3 = int.TryParse((splitedList[4]), out operand3);

            if (isPossible1 && isPossible2 && isPossible3)
            {
                switch (splitedList[1], splitedList[3])
                {
                    case ("+", "+"):
                        return (operand1 + operand2) + operand3;
                    case ("+", "-"):
                        return (operand1 + operand2) - operand3;
                    case ("+", "*"):
                        return (operand1 + operand2) * operand3;
                    case ("+", "/"):
                        return (operand1 + operand2) / operand3;

                    case ("-", "+"):
                        return (operand1 - operand2) + operand3;
                    case ("-", "-"):
                        return (operand1 - operand2) - operand3;
                    case ("-", "*"):
                        return (operand1 - operand2) * operand3;
                    case ("-", "/"):
                        return (operand1 - operand2) / operand3;

                    case ("*", "+"):
                        return (operand1 * operand2) + operand3;
                    case ("*", "-"):
                        return (operand1 * operand2) - operand3;
                    case ("*", "*"):
                        return (operand1 * operand2) * operand3;
                    case ("*", "/"):
                        return (operand1 * operand2) / operand3;

                    case ("/", "+"):
                        return (operand1 * operand2) + operand3;
                    case ("/", "-"):
                        return (operand1 * operand2) - operand3;
                    case ("/", "*"):
                        return (operand1 * operand2) * operand3;
                    case ("/", "/"):
                        return (operand1 / operand2) / operand3;

                    default:
                        throw new NotImplementedException("You need to implement this function.");
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }
        throw new ArgumentException();
    }
}