using System;

public static class Series
{
    public static string[] Slices(string numbers, int sliceLength)
    {
        var maxLength = numbers.Length - (sliceLength - 1);
        var result = new string[maxLength];

        if (sliceLength > numbers.Length || sliceLength < 1)
            throw new ArgumentException();

        for (int i = 0, length = (numbers.Length - sliceLength) + 1; i < length; i++)
        {
            result[i] = (numbers.Substring(i, sliceLength));
        }
        return result;
    }
}