using System;

public class Matrix
{
    int rowLength = 0;
    int columnLength = 0;
    int[,] matrixCollection = new int[0, 0];

    public Matrix(string input)
    {
        var inputCollectionSeries = input.Split("\n");
        input = input.Replace("\n", " ");
        var inputCollectionValues = input.Split(' ');
        rowLength = inputCollectionSeries.Length;
        columnLength = inputCollectionSeries[0].Split(" ").Length;
        int[,] matrixCollection = new int[rowLength, columnLength];
        var indexCount = 0;
        for (int i = 0; i < rowLength; i++)
        {
            for (int j = 0; j < columnLength; j++)
            {
                matrixCollection[i, j] = Convert.ToInt16(inputCollectionValues[indexCount]);
                indexCount++;
            }
        }
        this.matrixCollection = matrixCollection;
    }

    public int[] Row(int row)
    {
        row--;
        var rowValues = new int[columnLength];
        for (int i = 0; i < columnLength; i++)
        {
            rowValues[i] = (matrixCollection[row, i]);
        }
        return rowValues;
    }

    public int[] Column(int col)
    {
        col--;
        var columnValues = new int[rowLength];
        for (int i = 0; i < rowLength; i++)
        {
            columnValues[i] = (matrixCollection[i, col]);
        }
        return columnValues;
    }
}