using System.Text;

public static class Acronym
{
    public static string Abbreviate(string phrase)
    {
        var abbreviateBuilder = new StringBuilder();
        var letterToRemove = "!";

        phrase = phrase.Replace("-", " ").ToUpper();

        for (int i = 0, length = phrase.Length; i < length; i++)
        {
            if ((phrase[i] < 'A' || phrase[i] > 'Z') && phrase[i] != ' ')
            {
                phrase = phrase.Replace(phrase[i].ToString(), letterToRemove);
            }
        }

        var correctedPhaseItems = phrase.Replace(letterToRemove, string.Empty).Split(' ');

        for (int i = 0, length = correctedPhaseItems.Length; i < length; i++)
        {
            if (string.IsNullOrEmpty(correctedPhaseItems[i]))
            {
                continue;
            }

            // [0] is the first letter in the word
            abbreviateBuilder.Append(correctedPhaseItems[i][0]);
        }

        return abbreviateBuilder.ToString();
    }
}