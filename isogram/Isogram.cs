using System;
using System.Collections.Generic;

public static class Isogram
{
    public static bool IsIsogram(string word)
    {
        var collections = new HashSet<char>();

        var newWord = word.ToLower();
        newWord = newWord.Replace(" ", "");
        newWord = newWord.Replace("-", "");

        for (int i = 0; i < newWord.Length; i++)
        {
            collections.Add(newWord[i]);
        }
        return newWord.Length == collections.Count;
    }
}
