using System;

public class SpaceAge
{
    private int seconds = 0;
    private int secondPerday = 24 * 60 * 60;

    private float daysPerYear = 365.25f;

    private float mercury = 0.2408467f;

    private float venus = 0.61519726f;

    private float mars = 1.8808158f;

    private float jupiter = 11.862615f;

    private float saturn = 29.447498f;

    private float uranus = 84.016846f;

    private float neptune = 164.79132f;

    public SpaceAge(int seconds)
    {
        this.seconds = seconds;
    }

    public double OnEarth() => (seconds / (daysPerYear * secondPerday));

    public double OnMercury() => (seconds / ((mercury * daysPerYear) * secondPerday));

    public double OnVenus() => (seconds / ((venus * daysPerYear) * secondPerday));

    public double OnMars() => (seconds / ((mars * daysPerYear) * secondPerday));

    public double OnJupiter() => (seconds / ((jupiter * daysPerYear) * secondPerday));

    public double OnSaturn() => (seconds / ((saturn * daysPerYear) * secondPerday));

    public double OnUranus() => (seconds / ((uranus * daysPerYear) * secondPerday));

    public double OnNeptune() => (seconds / ((neptune * daysPerYear) * secondPerday));

}