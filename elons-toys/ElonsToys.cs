using System;

class RemoteControlCar
{
    private int meterPerDrive = 20;
    private int totalMeterDrive = 0;
    private int remainBattery = 100;
    public static RemoteControlCar Buy()
    {
        return new RemoteControlCar();
        //throw new NotImplementedException("Please implement the (static) RemoteControlCar.Buy() method");
    }

    public string DistanceDisplay()
    {
        return $"Driven {totalMeterDrive} meters";
//    throw new NotImplementedException("Please implement the RemoteControlCar.DistanceDisplay() method");
    }

    public string BatteryDisplay()
    {
        var result = remainBattery > 0 ? $"Battery at {remainBattery}%" : "Battery empty";
        return result;
       // throw new NotImplementedException("Please implement the RemoteControlCar.BatteryDisplay() method");
    }

    public void Drive()
    {
        var battery = 100;
        if(totalMeterDrive <= meterPerDrive * battery && remainBattery > 0)
        totalMeterDrive += meterPerDrive;
        --remainBattery;

        //throw new NotImplementedException("Please implement the RemoteControlCar.Drive() method");
    }
}
