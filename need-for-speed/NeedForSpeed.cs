class RemoteControlCar
{
    private int speed = 0;
    private int drive = 0;
    private int batteryDrain = 0;
    private int totalBattery = 100;


    public RemoteControlCar(int speed, int batteryDrain)
    {
        this.speed = speed;
        this.batteryDrain = batteryDrain;
    }

    public bool BatteryDrained() => (totalBattery < batteryDrain);

    public int DistanceDriven() => drive;

    public void Drive()
    {
        if (totalBattery >= batteryDrain)
        {
            drive += speed;
        }
        totalBattery -= batteryDrain;
    }

    public static RemoteControlCar Nitro()
    {
        var meterOfNitro = 50;
        var batteryDrainOfNitro = 4;
        return new RemoteControlCar(meterOfNitro, batteryDrainOfNitro);
    }
}

class RaceTrack
{
    private int distance = 0;

    public RaceTrack(int distance)
    {
        this.distance = distance;
    }

    public bool TryFinishTrack(RemoteControlCar car)
    {
        while (!(car.BatteryDrained()))
        {
            car.Drive();
        }
        return (car.DistanceDriven() >= distance);
    }
}
