public static class Triangle
{
    public static bool IsScalene(double side1, double side2, double side3)
    {
        if (IsTriangle(side1, side2, side3))
        {
            return ((side1 != side2) && (side2 != side3) && (side3 != side1));
        }
        return false;
    }

    public static bool IsIsosceles(double side1, double side2, double side3)
    {
        if (IsTriangle(side1, side2, side3))
        {
            return ((side1 == side2) || (side2 == side3) || (side3 == side1));
        }
        return false;
    }

    public static bool IsEquilateral(double side1, double side2, double side3)
    {
        if (IsTriangle(side1, side2, side3))
        {
            return ((side1 == side2) && (side2 == side3) && (side3 == side1));
        }
        return false;
    }

    private static bool IsTriangle(double A, double B, double C)
    {
        return ((A + B >= C && B + C >= A && A + C >= B) && (A > 0 && B > 0 && C > 0));
    }
}