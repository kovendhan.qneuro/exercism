using System;

public class Orm
{
    private Database database;

    public Orm(Database database)
    {
        this.database = database;
    }

    public void Begin()
    {
        database.BeginTransaction();
        //throw new NotImplementedException($"Please implement the Orm.Begin() method");
    }

    public void Write(string data)
    {
        database.Write(data);
        //throw new NotImplementedException($"Please implement the Orm.Write() method");
    }

    public void Commit()
    {
        database.EndTransaction();
        //throw new NotImplementedException($"Please implement the Orm.Commit() method");
    }
}