using System;
using System.Collections.Generic;

public static class NucleotideCount
{
    public static IDictionary<char, int> Count(string sequence)
    {
        var nucleotideCollections = new Dictionary<char, int>()
        {
            ['A'] = 0,
            ['C'] = 0,
            ['G'] = 0,
            ['T'] = 0,
        };

        for (int i = 0, sequenceLength = sequence.Length; i < sequenceLength; i++)
        {
            if (nucleotideCollections.ContainsKey(sequence[i]))
            {
                nucleotideCollections[sequence[i]] += 1;
            }
            else
            {
                throw new ArgumentException();
            }
        }
        return nucleotideCollections;
    }
}