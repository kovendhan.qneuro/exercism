using System;

public static class TelemetryBuffer
{
    public static byte[] ToBuffer(long reading)
    {
        return System.Text.Encoding.ASCII.GetBytes(reading.ToString());
    }

    public static long FromBuffer(byte[] buffer)
    {
        return BitConverter.ToInt64(buffer);
    }
}
