using System;

class WeighingMachine
{
    private int precision = 0;

    private double weight = 0;

    private double tareAdjustment = 5.0;

    private float finalWeight = 0;

    public WeighingMachine(int precision)
    {
        this.precision = precision;
    }

    public int Precision
    {
        get => precision;
        set => precision = value;
    }

    public double Weight
    {
        get => weight;
        set
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            weight = value;
        }
    }

    public double TareAdjustment
    {
        get => tareAdjustment;
        set => tareAdjustment = value;
    }

    public string DisplayWeight
    {
        get
        {
            var temp = string.Format("{0:F3}", Weight - TareAdjustment);
            return $"{temp} kg";
        }
    }
}
