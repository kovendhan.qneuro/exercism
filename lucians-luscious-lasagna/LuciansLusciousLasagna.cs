class Lasagna
{
    private int timeInOven = 40;
    private int timePerLayer = 2;
    public int ExpectedMinutesInOven(){
        return timeInOven;
    }

    // TODO: define the 'RemainingMinutesInOven()' method
    public int RemainingMinutesInOven( int cookedTime){
        return timeInOven - cookedTime;
    }

    // TODO: define the 'PreparationTimeInMinutes()' method
    public int PreparationTimeInMinutes(int layer){
        return layer * timePerLayer;
    }

    // TODO: define the 'ElapsedTimeInMinutes()' method
    public int ElapsedTimeInMinutes(int layer,int timeCooked){
        var result = layer * timePerLayer + timeCooked;
        return result;
    }
}
