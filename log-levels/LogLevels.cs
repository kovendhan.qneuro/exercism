using System;

static class LogLine
{
    public static string Message(string logLine)
    {
        string result = string.Empty;
        if (logLine.Contains("[ERROR]:"))
        {
            result =logLine.Replace("[ERROR]:"," ");
        }
        else if (logLine.Contains("[WARNING]:"))
        {
            result = logLine.Replace("[WARNING]:"," ");
        }
        else if(logLine.Contains("[INFO]:"))
        {
            result = logLine.Replace("[INFO]:"," ");
        }
        return result.Trim();

        //throw new NotImplementedException("Please implement the (static) LogLine.Message() method");
    }

    public static string LogLevel(string logLine)
    {
        if (logLine.Contains("[ERROR]:"))
        {
            return "error";
        }
        else if (logLine.Contains("[WARNING]:"))
        {
            return "warning";
        }
        else //if(logLine.Contains("[INFO]:"))
        {
            return "info";
        }       
        //throw new NotImplementedException("Please implement the (static) LogLine.LogLevel() method");
    }

    public static string Reformat(string logLine)
    {
        var result = $"{LogLine.Message(logLine)} ({LogLine.LogLevel(logLine)})";
        return result;
        //throw new NotImplementedException("Please implement the (static) LogLine.Reformat() method");
    }
}
