using System.Text;

public static class Identifier
{
    public static string Clean(string identifier)
    {
        var stringBuilder = new StringBuilder();
        for(int i =0;i< identifier.Length;i++)
        {
            if(identifier[i] == '-')
            {
                i++;
                stringBuilder.Append(char.ToUpper(identifier[i]));
            }
            else if(char.IsWhiteSpace(identifier[i]))
            {
                stringBuilder.Append('_');
            }
            else if(char.IsControl(identifier[i]))
            {
                stringBuilder.Append("CTRL");
            }
            else if(identifier[i] >= 'α' && identifier[i] <= 'ω')
            {
                continue;
            }
            else if(!char.IsLetter(identifier[i]))
            {
                continue;
            }
            else
            {
                stringBuilder.Append(identifier[i]);
            }
        }
        return stringBuilder.ToString();
    }
}
