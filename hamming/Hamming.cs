using System;

public static class Hamming
{
    public static int Distance(string firstStrand, string secondStrand)
    {
        var count = 0;

        if(firstStrand.Length != secondStrand.Length)
        {
            throw new ArgumentException();
        }

        for(int i =0, lengthOfstring = firstStrand.Length; i < lengthOfstring ; i++)
        {
            if(firstStrand[i] != secondStrand[i])
            {
                count++;
            }
        }
        return count;
    }
}