using System.Collections.Generic;
using System.Linq;

public class GradeSchool
{
    private Dictionary<string, int> collections = new Dictionary<string, int>();

    public void Add(string student, int grade)
    {
        collections.Add(student, grade);
    }

    public IEnumerable<string> Roster()
    {
        var rosterList = new List<string>();
        foreach (var items in collections.OrderBy(i => i.Value).ThenBy(i => i.Key))
        {
            rosterList.Add(items.Key);
        }
        return rosterList;
    }

    public IEnumerable<string> Grade(int grade)
    {
        var gradeList = new List<string>();
        foreach (var items in collections.OrderBy(i => i.Key))
        {
            if (items.Value == grade)
            {
                gradeList.Add(items.Key);
            }
        }
        return gradeList;
    }
}