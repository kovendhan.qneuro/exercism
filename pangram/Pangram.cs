public static class Pangram
{
    public static bool IsPangram(string input)
    {
        input = input.ToLower();
        var letterMap = new int[26];

        for (int i = 0, length = input.Length; i < length; i++)
        {
            if ((input[i] >= 97 && input[i] <= 122))
            {
                letterMap[input[i] - 'a'] = 1;
            }
        }

        for (int i = 0; i < 26; i++)
        {
            if (letterMap[i] == 0)
            {
                return false;
            }
        }
        return true;
    }
}
