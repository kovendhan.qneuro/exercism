using System;
using System.Collections.Generic;

public class Anagram
{
    private string baseWord = string.Empty;

    public Anagram(string baseWord)
    {
        this.baseWord = baseWord.ToUpper();
    }

    public string[] FindAnagrams(string[] potentialMatches)
    {
        var potentialMatchClone = new string[potentialMatches.Length];
        var baseWordClone = baseWord.ToCharArray();
        var anagramList = new List<string>();

        Array.Sort(baseWordClone);
        for (int i = 0, length = potentialMatches.Length; i < length; i++)
        {
            potentialMatchClone[i] = potentialMatches[i].ToUpper();
        }
        for (int i = 0, length = potentialMatchClone.Length; i < length; i++)
        {
            var baseLength = baseWord.Length;
            if (baseLength == potentialMatchClone[i].Length)
            {
                var count = 0;
                for (int j = 0; j < baseLength; j++)
                {
                    var tempString = potentialMatchClone[i].ToCharArray();
                    Array.Sort(tempString);
                    if (baseWordClone[j] == tempString[j])
                        count++;
                }
                if (count == baseLength && !baseWord.Equals(potentialMatchClone[i]))
                    anagramList.Add(potentialMatches[i]);
            }
        }
        return anagramList.ToArray();
    }
}