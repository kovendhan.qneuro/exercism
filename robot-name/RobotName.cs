using System;
using System.Collections.Generic;

public class Robot
{
    private static HashSet<string> nameList = new();

    private string newName = string.Empty;

    public Robot()
    {
        Reset();
    }

    public string Name
    {
        get
        {
            return newName;
        }
        set
        {
            newName = value;
        }
    }

    public void Reset() => newName = generateName();

    public string generateName()
    {
        var random = new Random();
        var tempName = $"{(char)random.Next('A', '[')}{(char)random.Next('A', 'Z')}{random.Next(0, 9)}{random.Next(0, 9)}{random.Next(0, 9)}";

        return nameList.Add(tempName) ? tempName : generateName();
    }
}