public enum Direction
{
    North,
    East,
    South,
    West
}

public class RobotSimulator
{
    public Direction Direction { get; set; }

    public int X { get; set; }

    public int Y { get; set; }

    public RobotSimulator(Direction direction, int x, int y)
    {
        X = x;
        Y = y;
        Direction = direction;
    }

    public void Move(string instructions)
    {
        for (int i = 0, length = instructions.Length; i < length; i++)
        {
            var command = instructions[i];
            if (Direction == Direction.North)
            {
                switch (command)
                {
                    case 'R':
                        Direction = Direction.East;
                        break;

                    case 'L':
                        Direction = Direction.West;
                        break;

                    case 'A':
                        Y++;
                        break;
                }
            }
            else if (Direction == Direction.South)
            {
                switch (command)
                {
                    case 'R':
                        Direction = Direction.West;
                        break;

                    case 'L':
                        Direction = Direction.East;
                        break;

                    case 'A':
                        Y--;
                        break;
                }
            }
            else if (Direction == Direction.East)
            {
                switch (command)
                {
                    case 'R':
                        Direction = Direction.South;
                        break;

                    case 'L':
                        Direction = Direction.North;
                        break;

                    case 'A':
                        X++;
                        break;
                }
            }
            else
            {
                switch (command)
                {
                    case 'R':
                        Direction = Direction.North;
                        break;

                    case 'L':
                        Direction = Direction.South;
                        break;

                    case 'A':
                        X--;
                        break;
                }
            }
        }
    }
}