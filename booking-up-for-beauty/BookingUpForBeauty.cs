using System;
static class Appointment
{
    public static DateTime Schedule(string appointmentDateDescription)
    {
        return (DateTime.Parse(appointmentDateDescription));
    }

    public static bool HasPassed(DateTime appointmentDate)
    {
        return !(appointmentDate > DateTime.Now);
    }

    public static bool IsAfternoonAppointment(DateTime appointmentDate)
    {
        return (appointmentDate.Hour >= 12.00 && appointmentDate.Hour < 18.00);
    }

    public static string Description(DateTime appointmentDate)
    {
        return $"You have an appointment on {appointmentDate}.";
    }

    public static DateTime AnniversaryDate()
    {
        var aniversaryDate = new DateTime(DateTime.Now.Year, 9, 15);
        return aniversaryDate;
    }
}
