public static class RnaTranscription
{
    public static string ToRna(string nucleotide)
    {
        var nucleotideLength = nucleotide.Length;
        var rnaString = new char[nucleotideLength];
        for (int i = 0; i < nucleotideLength; i++)
        {
            rnaString[i] = nucleotide[i] switch
            {
                'G' => 'C',
                'C' => 'G',
                'T' => 'A',
                'A' => 'U',
                _ => '\0'
            };
        }
        return new string(rnaString);
    }
}