public static class Rectangles
{
    public static int Count(string[] rows)
    {
        var rowsLength = rows.Length;
        if (rowsLength == 0)
            return 0;
        var columnLength = rows[0].Length;
        var rectangleCount = 0;
        for (int i = 0; i < rowsLength; i++)
        {
            for (int j = 0; j < columnLength; j++)
            {
                if (rows[i][j] == '+')
                {
                    for (int l = i + 1; l < rowsLength; l++)
                    {
                        for (int k = j + 1; k < columnLength; k++)
                        {
                            if (rows[l][j] == '+' && rows[i][k] == '+' && rows[l][k] == '+')
                            {
                                rectangleCount++;
                                for (int x = i + 1; x < l; x++)
                                {
                                    if (rows[x][j] == '-' || rows[x][j] == ' ' || rows[x][k] == '-' || rows[x][k] == ' ')
                                    {
                                        rectangleCount--;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return rectangleCount;
    }
}