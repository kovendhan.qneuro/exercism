using System.Collections.Generic;

public static class PythagoreanTriplet
{
    public static IEnumerable<(int a, int b, int c)> TripletsWithSum(int sum)
    {
        var tripletsList = new List<(int, int, int)>();
        var length = sum/2;
        for (int a = 1; a <= length; a++)
        {
            for (int b = 1; b <= length; b++)
            {
                var c = sum - a - b;
                if (((a * a) + (b * b) == (c * c)) && a < b && b < c)
                {
                    tripletsList.Add((a, b, c));
                }
            }
        }
        return tripletsList;
    }
}