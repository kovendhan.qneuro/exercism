using System;
public static class ArmstrongNumbers
{
    public static bool IsArmstrongNumber(int number)
    {
        var stringNumber = number.ToString();
        var finalCount = 0;
        for (int i = 0, length = stringNumber.Length; i < length; i++)
        {
            finalCount += (int)Math.Pow((int)((stringNumber[i])-'0'), length);
        }
        return finalCount == number;
    }
}