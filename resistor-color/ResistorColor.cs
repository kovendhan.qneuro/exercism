using System;

public static class ResistorColor
{
    private enum allColors
    {
        black = 0,
        brown = 1,
        red = 2,
        orange = 3,
        yellow = 4,
        green = 5,
        blue = 6,
        violet = 7,
        grey = 8,
        white = 9
    }

    private static allColors selectedColor;

    public static int ColorCode(string color)
    {
        foreach (string item in Enum.GetNames(typeof(allColors)))
        {
            if (item == color)
            {
                return (int)Enum.Parse(typeof(allColors), item);
            }
        }
        return 3;
    }

    public static string[] Colors() => Enum.GetNames(typeof(allColors));
}